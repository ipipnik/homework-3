//
//  MultOperation.m
//  Homework3
//
//  Created by kravinov on 11/11/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "MultOperation.h"

@implementation MultOperation

- (double)performWithSecondArgument:(double)secondArgument{
    return self.firstArgument * secondArgument;
}

@end
