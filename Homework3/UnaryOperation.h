//
//  UnaryOperation.h
//  Homework3
//
//  Created by kravinov on 11/11/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnaryOperation : NSObject

- (double) perform:(double)value;

@end
