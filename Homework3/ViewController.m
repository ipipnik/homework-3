//
//  ViewController.m
//  Homework3
//
//  Created by kravinov on 11/9/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    self.model = [[CalculatorModel alloc] init];
}

- (IBAction)pressDigit:(UIButton *)sender {
    
    _recentInput = sender.currentTitle;

    
    if(self.isNumberInputting){
        if(![sender.currentTitle isEqualToString:@","] || ![self.numbericLabel.text containsString:@","]){
            self.numbericLabel.text = [self.numbericLabel.text stringByAppendingString:sender.currentTitle];
        }
    }
    else{
        if([sender.currentTitle isEqualToString:@","]){
            self.numbericLabel.text = [NSString stringWithFormat:@"0,"];
        }
        else{
            self.numbericLabel.text = sender.currentTitle;
        }
        self.numberInputting = YES;
    }
    
}

- (IBAction)performOperation:(UIButton *)sender {
    
    self.numberInputting = NO;
    double value = self.numbericLabel.text.doubleValue;
    NSString* operationString = sender.currentTitle;
    
    // Фикс бинарных операций
    if (_recentInput != operationString){
        double result = [self.model performOperation:operationString withValue:value];
        _recentInput = operationString;
        self.numbericLabel.text = [@(result) stringValue];
    }
    // ---
}

@end
