//
//  BinaryOperation.h
//  Homework3
//
//  Created by kravinov on 11/11/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BinaryOperation : NSObject

@property (nonatomic, assign) double firstArgument;

- (double) performWithSecondArgument:(double)secondArgument;

@end
