//
//  CosOperation.m
//  Homework3
//
//  Created by kravinov on 11/11/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "CosOperation.h"

@implementation CosOperation

- (double) perform:(double)value {
    return cos(value);
}

@end
