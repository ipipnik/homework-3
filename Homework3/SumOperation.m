//
//  SumOperation.m
//  Homework3
//
//  Created by kravinov on 11/11/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "SumOperation.h"

@implementation SumOperation

- (double)performWithSecondArgument:(double)secondArgument {
    return self.firstArgument + secondArgument;
}

@end
