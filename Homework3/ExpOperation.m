//
//  ExpOperation.m
//  Homework3
//
//  Created by kravinov on 11/11/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "ExpOperation.h"

@implementation ExpOperation

- (double) perform:(double)value {
    return exp(value);
}

@end
