//
//  UnaryOperation.m
//  Homework3
//
//  Created by kravinov on 11/11/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "UnaryOperation.h"

@implementation UnaryOperation

- (double) perform:(double)value {
    return value;
}

@end
