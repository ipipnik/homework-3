//
//  ViewController.h
//  Homework3
//
//  Created by kravinov on 11/9/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculatorModel.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *numbericLabel;
@property (nonatomic, assign, getter=isNumberInputting) BOOL numberInputting;
@property (nonatomic, strong) CalculatorModel *model;

@property (nonatomic, strong) NSString* recentInput;


@end

