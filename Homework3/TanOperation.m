//
//  TanOperation.m
//  Homework3
//
//  Created by kravinov on 11/11/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "TanOperation.h"

@implementation TanOperation

- (double) perform:(double)value {
    return tan(value);
}

@end
