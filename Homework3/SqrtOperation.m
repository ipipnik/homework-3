//
//  SqrtOperation.m
//  Homework3
//
//  Created by kravinov on 11/11/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "SqrtOperation.h"

@implementation SqrtOperation

- (double) perform:(double)value {
    return sqrt(value);
}

@end
