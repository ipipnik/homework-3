//
//  PerOperation.m
//  Homework3
//
//  Created by kravinov on 11/11/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "PerOperation.h"

@implementation PerOperation

- (double) perform:(double)value {
    return value / 100;
}

@end
