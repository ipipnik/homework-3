//
//  SinOperation.m
//  Homework3
//
//  Created by kravinov on 11/11/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "SinOperation.h"

@implementation SinOperation

- (double) perform:(double)value {
    return sin(value);
}

@end
