//
//  AppDelegate.h
//  Homework3
//
//  Created by kravinov on 11/9/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

