//
//  CalculatorModel.m
//  Homework3
//
//  Created by kravinov on 11/11/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "CalculatorModel.h"
#import "SumOperation.h"
#import "SingOperation.h"
#import "EqualOperation.h"

#import "DivOperation.h"
#import "MultOperation.h"
#import "SubOperation.h"
#import "PerOperation.h"
#import "ACOperation.h"
#import "SinOperation.h"
#import "CosOperation.h"
#import "SqrtOperation.h"
#import "TanOperation.h"
#import "ExpOperation.h"

@implementation CalculatorModel

- (instancetype) init {
    self = [super init];
    if (self) {
        _operations = @{
                        @"+" : [[SumOperation alloc] init],
                        @"±" : [[SingOperation alloc] init],
                        @"=" : [[EqualOperation alloc] init],
                        
                        @"-" : [[SubOperation alloc] init],
                        @"x" : [[MultOperation alloc] init],
                        @"÷" : [[DivOperation alloc] init],
                        @"%" : [[PerOperation alloc] init],
                       @"AC" : [[ACOperation alloc] init],
                      
                      @"sin" : [[SingOperation alloc] init],
                      @"cos" : [[CosOperation alloc] init],
                        @"√" : [[SqrtOperation alloc] init],
                      @"tan" : [[TanOperation alloc] init],
                      @"exp" : [[ExpOperation alloc] init],
                        };
    }
    return self;
}

- (id)operationForOperationString:(NSString*)operationString {
    id operation = [self.operations objectForKey:operationString];
    if (!operation) {
        operation = [[UnaryOperation alloc] init];
    }
    return operation;
}

- (double)performOperation:(NSString *)operationStrig withValue:(double)value {
    id operation = [self operationForOperationString:operationStrig];
    
    if([operation isKindOfClass:[EqualOperation class]]) {
        if(self.currentBinaryOperation) {
            value = [self.currentBinaryOperation performWithSecondArgument:value];
        }
    }
    
    if([operation isKindOfClass:[UnaryOperation class]]) {
        return [operation perform:value];
    }
    
    if([operation isKindOfClass:[BinaryOperation class]]) {
//        if(self.currentBinaryOperation) {
//            value = [self.currentBinaryOperation performWithSecondArgument:value];
//        }
        self.currentBinaryOperation = operation;
        self.currentBinaryOperation.firstArgument = value;
        return value;
    }
    return DBL_MAX;
}

@end
