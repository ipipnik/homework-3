//
//  CalculatorModel.h
//  Homework3
//
//  Created by kravinov on 11/11/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BinaryOperation.h"
#import "UnaryOperation.h"

@interface CalculatorModel : NSObject

@property (nonatomic, strong) NSDictionary* operations;
@property (nonatomic, strong) BinaryOperation* currentBinaryOperation;

- (double)performOperation:(NSString*)operationStig withValue:(double)value;

@end
